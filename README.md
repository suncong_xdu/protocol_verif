# ProVerif scripts for our authentication works

### Directory Intro

* Ridra - proof scripts for [1].

* j_comm - proof script for [4].

* j_xdu - proof scripts for [3].

* Jie_thesis - proof scripts for [5].

* Jiao_thesis - proof script for Section 3.3.4 of [6]. The complete proof combines the script in /j_comm.

### References

1. Cong Sun, Jiao Liu, Yinjuan Jie, Yuwan Ma, Jianfeng Ma. Ridra: A Rigorous Decentralized Randomized Authentication in VANETs. IEEE Access, 2018, 6: 50358-50371.
2. Cong Sun, Jiao Liu, Xinpeng Xu, Jianfeng Ma, A Privacy-Preserving Mutual Authentication Resisting DoS Attacks in VANETs. IEEE Access, 2017, 5: 24012-24022.
3. LIU Jiao, SUN Cong, MA Jianfeng, JIAO Zhengda, Remote authorization protocol for vehicle diagnosis using trusted platform measurement. Journal of Xidian University, 2017, 44(3): 49-54,119. (in Chinese)
4. JIAO Zhengda, MA Jianfeng, SUN Cong, YAO Qingsong, New remote authorization protocol for vehicle diagnosis. Journal on Communications, 2014, 35(11): 146-153. (in Chinese)
5. Jie Yinjuan, Research on Security Analysis and Proof Technology of Improved Extensible Authentication Protocol. Master thesis: Xidian University, 2020.
6. Jiao Zhengda, Design of Remote Authorization Protocol for Vehicle Diagnosis and Research on Formalized Verification. Master thesis: Xidian University, 2016.

### Maintainer

* **Cong Sun** - *School of Cyber Engineering, Xidian University* - [E-mail: suncong AT xidian DOT edu DOT cn]

### License

* All right researved.
