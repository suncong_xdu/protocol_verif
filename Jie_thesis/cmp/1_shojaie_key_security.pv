 (* 开启类型鉴别 *)
 set ignoreTypes = false .
 (*free c: channel .*)
 channel c,c1 .

  
 (* 对称加密 共享密钥*)
 type key . 
 fun senc(bitstring, key): bitstring .
 reduc forall m: bitstring, srk: key;
 sdec(senc(m, srk), srk) = m .
 

  (* 函数定义  *) 
 type nonce .
 type algo .
 type hamc_key .
 type master_key .
 
 fun XOR1(nonce,nonce):hamc_key .
 fun XOR2(bitstring,hamc_key):master_key .
 fun HMAC1(bitstring,key):bitstring .
 fun HMAC2(bitstring,hamc_key):bitstring .
 fun HMAC3(bitstring,master_key):bitstring .
 

 (* 非对称加密 采用RSA公钥加密 *)
 type pkey .
 type skey .

 fun pk(skey): pkey .
 fun aenc(bitstring, pkey): bitstring .
 reduc forall m: bitstring, sk: skey;
 adec(aenc(m, pk(sk)), sk) = m .
 

  (* 签名 *)
 type spkey .
 type sskey .
 type cert .
 
 fun spk(sskey): spkey .
 fun sign1(cert, sskey): bitstring .
 fun sign2(bitstring, sskey): bitstring .

 reduc forall m1: cert, ssk:sskey; getmess1(sign1(m1, ssk)) = m1 .
 reduc forall m1: cert, ssk:sskey; checksign1(sign1(m1, ssk), spk(ssk)) = m1 .
 
 reduc forall m2: bitstring, ssk:sskey; getmess(sign2(m2, ssk)) = m2 .
 reduc forall m2: bitstring, ssk:sskey; checksign2(sign2(m2, ssk), spk(ssk)) = m2 .
 

 (* 可信主体 *)
  type host .
  free S,C,CA: host.

  
 (* 数字证书 *)
  (* 证书 *)
 fun mkcert(bitstring, sskey): cert.
 reduc forall certificate_content: bitstring,ssk:sskey,spk:spkey;
 checkcert(mkcert(certificate_content, ssk), spk) = certificate_content.


 (* 安全和身份认证 *)
 type ID .
 free s: bitstring [private].
 free IDS: ID [private].
 

 (* 安全假设  看什么时候添加共享密钥 *)
 not attacker(new skS).
 not attacker(new skC).
 not attacker(new sskS).
 not attacker(new sskC).
 
 
 (* 事件定义 *)

 event client_first_stage_first_receive(pkey).
 event server_first_stage_first_send(pkey).
 
 event server_first_stage_second_receive(ID).
 event client_first_stage_second_send(ID).
 
 event client_four_stage_three_receive(bitstring).
 event server_four_stage_three_send(bitstring).
 
 
 
 
 
 (* 质疑 *)
  
 query pkS:pkey;
 event (client_first_stage_first_receive(pkS)) ==> event (server_first_stage_first_send(pkS)).


 
 
 
 
 
 
 (* 主体进程定义 *)
 
 let processCA(sskCA:sskey) =
 in(c, (m0S:bitstring));
 let (server_certificate_content:bitstring)= m0S in
 let server_certificate:cert = mkcert(server_certificate_content,sskCA)in
 out(c, (server_certificate));
 
 in(c, (m0C:bitstring));
 let (client_certificate_content:bitstring)= m0C in
 let client_certificate:cert = mkcert(client_certificate_content,sskCA)in
 out(c, (client_certificate)).
 
 
 
 let processS(IDS:ID,pkS:pkey,skS:skey,spkS:spkey,sskS:sskey) =
 
 (* 一阶段 Server *) (*发*)
 in(c1,(flag_byte:bitstring, date:bitstring, IDi:ID, session_ID:ID, cipher_suit: bitstring));
 let (EAP_rq:bitstring)=(flag_byte,date)in
 let TLS_start_packet:bitstring=(EAP_rq,IDi)in
 out(c, (TLS_start_packet,pkS,spkS));
 event server_first_stage_first_send(pkS);  
 
 (* 一阶段 Server *) (*收*)
 in(c, (m2:bitstring));
 let (EAP_response_packet:bitstring,client_hello:bitstring,pkC:pkey,spkC:spkey)= adec(m2,skS)in
 let (EAP_rp:bitstring,IDC:ID)= EAP_response_packet in
 event server_first_stage_second_receive(IDC); 
 
 let (client_version_number:bitstring,session_ID':ID,client_random_number:nonce,cipher_suit': bitstring,kSC:key)= client_hello in
 if session_ID'= session_ID then (
		 let finished_message_server_1:bitstring = HMAC1(TLS_start_packet,kSC) in
		 
		 (* 四阶段 Server *) (*发*)
		 out(c, (finished_message_server_1)); 		 
		 new EAP_success_message:bitstring;	
		 in(c, (EAP_TLS_packet:bitstring));
		 out(c, (EAP_success_message));
		 event server_four_stage_three_send(EAP_success_message))
 else(
         if cipher_suit'=cipher_suit then
		 in (c1, (server_version_number:bitstring, server_random_number:nonce));
		 let (server_hello:bitstring)=(server_version_number,session_ID,server_random_number,cipher_suit)in
		 in(c1, (server_certificate_version:bitstring, server_serial_number:bitstring, 
				 server_signature_algorithm:algo, server_issuer:bitstring, server_validity:bitstring, 
				 server_subject:bitstring, server_key_exchange: bitstring,certificate_type:bitstring, authority:bitstring));
		 let server_certificate_content:bitstring =(server_certificate_version,server_serial_number,server_signature_algorithm,server_issuer,server_validity,server_subject,pkS)in
		 out(c, (server_certificate_content));
         in(c, (m0S1:cert));
		 let server_certificate:cert = m0S1 in
		 let server_sign1:bitstring = sign1(server_certificate,sskS) in 
		 let server_sign2:bitstring = sign2(server_key_exchange, sskS) in  
		 let certificate_request:bitstring = (certificate_type,authority)in
		 let HMAC_key:hamc_key = XOR1(server_random_number,client_random_number)in
		 let HMAC_server:bitstring = HMAC2((server_hello,server_certificate,server_key_exchange,certificate_request),HMAC_key) in
		 
		 (* 二阶段 Server *) (*发*)
		 out(c, (HMAC_server,server_hello,server_certificate,server_key_exchange,certificate_request,server_sign1,server_sign2));
		 
		 (* 三阶段 Server *) (*收*)
		 in(c,(m4:bitstring));
		 let(finished_message_client:bitstring,client_sign1:bitstring,client_certificate:cert,senc:bitstring,client_finished_label:bitstring) = m4 in
		 if client_certificate = checksign1(client_sign1,spkC)then 
		 let client_key_exchange:bitstring = sdec(senc,kSC)in
		 let client_certificate_verify:bitstring = (client_certificate,client_key_exchange) in
		 let client_certificate_verify_hmac:bitstring = HMAC2(client_certificate_verify,HMAC_key)in
		 let HMAC_server_done_hmac:bitstring = HMAC2(HMAC_server,HMAC_key)in
		 let m_key:master_key = XOR2(client_key_exchange,HMAC_key)in
		 let finished_message_client':bitstring = HMAC3((HMAC_server_done_hmac,client_certificate_verify_hmac,client_finished_label),m_key) in
		 if finished_message_client'=finished_message_client then
		 in(c1,(server_finished_label:bitstring));
		 let finished_message_server:bitstring = HMAC3((finished_message_client,server_finished_label),m_key) in
		 
		 (* 四阶段 Server *) (*发*)
		 out(c, (finished_message_server,server_finished_label));	
		 in(c, (EAP_TLS_packet:bitstring));
		 new EAP_success_message:bitstring;
		 out(c, (EAP_success_message));
		 event server_four_stage_three_send(EAP_success_message)
     ).
    
 
 
 let processC(pkC:pkey,spkC:spkey,sskC:sskey,spkCA:spkey) =
 
 (* 一阶段 Client *) (*收*)
 in(c, (m1:bitstring));
 let (TLS_start_packet:bitstring, pkS:pkey, spkS:spkey) = m1 in
 event client_first_stage_first_receive(pkS);
 let (EAP_rq:bitstring,IDi:ID) = TLS_start_packet in
 let (flag_byte:bitstring,date:bitstring)= EAP_rq in
 in(c1, (shisan:bitstring));
 if flag_byte = shisan then
 
  (* 一阶段 Client *) (*发*)
 in(c1, (EAP_rp:bitstring, client_version_number:bitstring, session_ID':ID, client_random_number:nonce, cipher_suit': bitstring, kSC:key));
 new IDC:ID;
 let (EAP_response_packet:bitstring)=(EAP_rp,IDC)in
 let (client_hello:bitstring)=(client_version_number,session_ID',client_random_number,cipher_suit',kSC)in
 out(c, (aenc((EAP_response_packet,client_hello,pkC,spkC),pkS)));
 event client_first_stage_second_send(IDC);

 (* 二阶段 Client *) (*收*)
 let session_ID:ID suchthat session_ID'= session_ID in(		  
         in(c1, (EAP_TLS_packet:bitstring));
         in(c, (finished_message_server_1:bitstring));
		 if finished_message_server_1 = HMAC1(TLS_start_packet,kSC) then
		 out(c, (EAP_TLS_packet));
		 in(c, (EAP_success_message:bitstring));
		 event client_four_stage_three_receive(EAP_success_message))  
 else(   
		 in(c,(m3:bitstring));
		 let(HMAC_server:bitstring,server_hello:bitstring,server_certificate:cert,server_key_exchange:bitstring,certificate_request:bitstring,server_sign1:bitstring,server_sign2:bitstring) = m3 in
		 let (server_version_number:bitstring,session_ID:ID,server_random_number:nonce,cipher_suit:bitstring) = server_hello in
		 let HMAC_key:hamc_key = XOR1(server_random_number,client_random_number)in
		 let HMAC_client:bitstring = HMAC2((server_hello,server_certificate,server_key_exchange,certificate_request),HMAC_key) in
		 if HMAC_client = HMAC_server then 
		 let (server_certificate_content:bitstring) = checkcert(server_certificate,spkCA) in
		 let (server_certificate_version:bitstring,server_serial_number:bitstring,server_signature_algorithm:algo,
		      server_issuer:bitstring,server_validity:bitstring,server_subject:bitstring,pkS':pkey) = server_certificate_content in
		 if  pkS'=pkS then
		 if server_certificate = checksign1(server_sign1,spkS)then
		 if server_key_exchange = checksign2(server_sign2,spkS)then
		 let(certificate_type:bitstring,authority:bitstring)= certificate_request in
		 in(c1, (client_certificate_version:bitstring, client_serial_number:bitstring,
		         client_signature_algorithm:algo, client_issuer:bitstring, client_validity:bitstring,  client_subject:bitstring));
		 let client_certificate_content:bitstring =(client_certificate_version, client_serial_number,client_signature_algorithm, client_issuer, client_validity, client_subject)in
		 out(c, (client_certificate_content));
         in(c, (m0C1:cert));
		 let client_certificate:cert = m0C1 in
		 let client_sign1:bitstring = sign1(client_certificate,sskC) in 	 
		 let HMAC_server_done_hmac:bitstring = HMAC2(HMAC_server,HMAC_key)in 
		 in(c1, (client_key_exchange:bitstring, client_finished_label:bitstring));
		 let senc:bitstring = senc(client_key_exchange, kSC) in
		 let client_certificate_verify:bitstring = (client_certificate,client_key_exchange)in
		 let client_certificate_verify_hmac:bitstring = HMAC2(client_certificate_verify,HMAC_key)in 
		 let m_key:master_key = XOR2(client_key_exchange,HMAC_key)in
		 let finished_message_client:bitstring = HMAC3((HMAC_server_done_hmac,client_certificate_verify_hmac,client_finished_label),m_key) in
		 
		  (* 三阶段 Client *) (*发*)
		 out(c, (finished_message_client,client_sign1,client_certificate,senc,client_finished_label));

		 
		  (* 四阶段 Client *) (*收*)   
		 in(c, (m5:bitstring));
		 let(finished_message_server:bitstring,server_finished_label:bitstring) = m5 in
		 if finished_message_server = HMAC3((finished_message_client,server_finished_label),m_key) then
		 in(c1, (EAP_TLS_packet:bitstring));
		 out(c, (EAP_TLS_packet));
		 in(c, (EAP_success_message:bitstring));
         event client_four_stage_three_receive(EAP_success_message)
      ).
	  
	  
	  
 (* Main *)
 process
 new IDS:ID;
 
  (* 创建CA *)
 new skCA: skey;
 let pkCA=pk(skCA)in out(c, pkCA);
 new sskCA: sskey;
 let spkCA=spk(sskCA)in out(c, spkCA);
 
 (* 创建S *)
 new skS: skey;
 let pkS=pk(skS)in out(c, pkS);
 new sskS: sskey;
 let spkS=spk(sskS)in out(c, spkS);
 
 (* 创建C *)
 new skC: skey;
 let pkC=pk(skC)in out(c, pkC);
 new sskC: sskey;
 let spkC=spk(sskC)in out(c, spkC);
 
 (* 启动协议 *)
 (!(processCA(sskCA)) | !(processS(IDS,pkS,skS,spkS,sskS)) | !(processC(pkC,spkC,sskC,spkCA)))
 
 
 
 
 
 
 
 
 